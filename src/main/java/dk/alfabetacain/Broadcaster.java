package dk.alfabetacain;

import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Set;

public class Broadcaster {

    public static final String DEFAULT_GROUP = "234.42.42.42";
    public static final int DEFAULT_PORT = 42000;
    private final List<DatagramChannel> channels;
    private final int port;
    private final String groupName;
    private Selector selector;
    private SocketAddress groupAddress;

    public Broadcaster(List<DatagramChannel> channels, int port, String groupName) {
        this.channels = channels;
        this.port = port;
        this.groupName = groupName;
    }

    public Broadcaster() {
        channels = new ArrayList<>();
        port = DEFAULT_PORT;
        groupName = DEFAULT_GROUP;
    }

    public int open() throws IOException {
        selector = Selector.open();
        Enumeration<NetworkInterface> e = NetworkInterface.getNetworkInterfaces();
        while (e.hasMoreElements()) {
            NetworkInterface ni = e.nextElement();
            if (ni.isLoopback() || !ni.supportsMulticast())
                continue;
            try {
                DatagramChannel channel = DatagramChannel.open(StandardProtocolFamily.INET)
                        .setOption(StandardSocketOptions.SO_REUSEADDR, true)
                        .bind(new InetSocketAddress(DEFAULT_PORT))
                        .setOption(StandardSocketOptions.IP_MULTICAST_IF, ni);
                channel.configureBlocking(false);
                channel.join(InetAddress.getByName(DEFAULT_GROUP), ni);
                channels.add(channel);
                SelectionKey key = channel.register(selector, SelectionKey.OP_READ);
            } catch (IOException e1) {
                //e1.printStackTrace();
            }
        }
        groupAddress = new InetSocketAddress(groupName, port);
        return channels.size();
    }

    public void send(ByteBuffer msg) throws IOException {
        for (DatagramChannel channel : channels) {
            System.out.println("Sending message");
            channel.send(msg, groupAddress);
        }
    }

    public void close() {
        for (DatagramChannel channel : channels) {
            try {
                channel.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Remember to actually remove items when going through the selected ones
     *
     * @return The set of channels which have input
     * @throws IOException If .select() throws an exception
     */
    public Set<SelectionKey> select() throws IOException {
        while (true) {
            int count = selector.select();
            if (count == 0)
                continue;
            return selector.selectedKeys();
        }
    }
}