package dk.alfabetacain;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

/**
 * Created by christian on 02-09-2016.
 */
public class CallMeMessage extends SimpleMessage {
    private final int port;
    private final String name;

    public CallMeMessage(int port, String name) {
        this.port = port;
        this.name = name;
    }

    public CallMeMessage(ByteBuffer buffer) {
        int typ = buffer.getInt();
        assert(typ == SimpleMessage.CALL_ME_MESSAGE_TYPE);
        this.port = buffer.getInt();
        int length = buffer.getInt();
        if (length == 0)
            this.name = null;
        else {
            byte[] nameBytes = new byte[length];
            buffer.get(nameBytes);
            this.name = new String(nameBytes, Charset.forName("UTF-8"));
        }
    }

    public int getPort() {
        return port;
    }

    public String getName() {
        return name;
    }

    @Override
    public byte[] getBytes() {
        return getByteBuffer().array();
    }

    @Override
    public ByteBuffer getByteBuffer() {
        byte[] nameBytes = name == null ? new byte[0] : name.getBytes(Charset.forName("UTF-8"));
        int nameLength = nameBytes.length;
        ByteBuffer buffer = ByteBuffer.allocate(4 + 4 + 4 + nameLength);
        buffer.putInt(super.CALL_ME_MESSAGE_TYPE).putInt(port).putInt(nameLength);
        buffer.put(nameBytes);
        buffer.flip();
        return buffer;
    }
}
