package dk.alfabetacain;

/**
 * Created by christian on 29-08-2016.
 */
public class IDAndPortMessage {
    private final int port;
    private final String id;

    public IDAndPortMessage(int port, String id) {
        this.port = port;
        this.id = id;
    }

    public int getPort() {
        return port;
    }

    public String getId() {
        return id;
    }
}
