package dk.alfabetacain;

import org.omg.PortableServer.ID_ASSIGNMENT_POLICY_ID;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

/**
 * Created by christian on 29-08-2016.
 */
public class MessageFactory {

    public static final int ID_AND_PORT_MESSAGE_TYPE = 0;

    public static final ByteBuffer buildPortAndIdMessage(String id, int port) {
        byte[] bytes = id.getBytes(Charset.forName("UTF-8"));
        ByteBuffer buffer = ByteBuffer.allocate(4 + 4 + 4 + bytes.length);
        buffer.putInt(ID_AND_PORT_MESSAGE_TYPE).putInt(port).putInt(bytes.length).put(bytes).rewind();
        return buffer;
    }

    public static final IDAndPortMessage readIdAndPortMessage(ByteBuffer buffer) throws UnsupportedEncodingException {
        int type = buffer.getInt();
        switch (type) {
            case ID_AND_PORT_MESSAGE_TYPE:
                int port = buffer.getInt();
                int size = buffer.getInt();
                System.out.println("Remaining " + buffer.remaining());
                System.out.println("Expected " + size);
                byte[] bytes = new byte[size];
                buffer.get(bytes);
                String id = new String(bytes, "UTF-8");
                return new IDAndPortMessage(port, id);
            default:
                return null;
        }
    }
}
