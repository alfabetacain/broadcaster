package dk.alfabetacain;

import java.nio.ByteBuffer;

/**
 * Created by christian on 02-09-2016.
 */
public abstract class SimpleMessage {
    public static final int TEXT_MESSAGE_TYPE = 0;
    public static final int CALL_ME_MESSAGE_TYPE = 1;

    public abstract byte[] getBytes();
    public abstract ByteBuffer getByteBuffer();
}
