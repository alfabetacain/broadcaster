package dk.alfabetacain;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

/**
 * Created by christian on 02-09-2016.
 */
public class TextMessage extends SimpleMessage {
    private final String message;

    public TextMessage(String message) {
        super();
        this.message = message;
    }

    public TextMessage(ByteBuffer msgBuffer) {
        super();
        int typ = msgBuffer.getInt();
        assert(typ == SimpleMessage.TEXT_MESSAGE_TYPE);
        int length = msgBuffer.getInt();
        byte[] bytes = new byte[length];
        msgBuffer.get(bytes);
        message = new String(bytes, Charset.forName("UTF-8"));
    }

    public String getMessage() {
        return message;
    }

    @Override
    public byte[] getBytes() {
        return getByteBuffer().array();
    }

    @Override
    public ByteBuffer getByteBuffer() {
        byte[] messageBytes = message.getBytes(Charset.forName("UTF-8"));
        int messageLength = messageBytes.length;
        ByteBuffer buffer = ByteBuffer.allocate(4 + 4 + messageLength);
        buffer.putInt(SimpleMessage.TEXT_MESSAGE_TYPE).putInt(messageLength);
        buffer.put(messageBytes);
        buffer.flip();
        return buffer;
    }
}
