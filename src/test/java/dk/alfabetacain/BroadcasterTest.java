package dk.alfabetacain;

import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.charset.Charset;
import java.util.Set;
import static org.junit.Assert.*;

/**
 * Created by christian on 28-08-2016.
 */
public class BroadcasterTest {

    private Broadcaster cut;

    private static final String ENCODING;
    private static final String MESSAGE;
    private static final ByteBuffer MESSAGE_BYTEBUFFER;
    private static final ByteBuffer RECEIVE_BYTEBUFFER;
    static {
        ENCODING = "UTF-8";
        MESSAGE = "Hello world!";
        byte[] bytes = MESSAGE.getBytes(Charset.forName(ENCODING));
        MESSAGE_BYTEBUFFER = ByteBuffer.wrap(bytes);
        RECEIVE_BYTEBUFFER = ByteBuffer.allocate(bytes.length);
    }

    @org.junit.Before
    public void setUp() throws Exception {
        cut = new Broadcaster();
        RECEIVE_BYTEBUFFER.rewind();
    }

    @org.junit.After
    public void tearDown() throws Exception {
        cut.close();
    }

    @org.junit.Test
    public void canOpenWithDefaults() throws Exception {
        cut.open();
    }

    @org.junit.Test
    public void canSendMessageToItself() throws Exception {
        int size = cut.open();
        cut.send(MESSAGE_BYTEBUFFER);
        Set<SelectionKey> keys = cut.select();
        assertTrue("All interface channels must receive a message", size == keys.size());
        for (SelectionKey key : keys) {
            DatagramChannel channel = (DatagramChannel) key.channel();
            channel.receive(RECEIVE_BYTEBUFFER);
            String msg = new String(RECEIVE_BYTEBUFFER.array(), ENCODING);
            assertTrue("Message sent and received must be identical", MESSAGE.equals(msg));
        }
    }

    @org.junit.Test
    public void canSendComplexMessageToSelf() throws Exception {
        int size = cut.open();
        String message = "Very important message!";
        byte[] messageBytes = message.getBytes(ENCODING);
        ByteBuffer messageBuffer = ByteBuffer.allocate(4 + 4 + messageBytes.length);
        messageBuffer.putInt(1);
        messageBuffer.putInt(messageBytes.length);
        messageBuffer.put(messageBytes);
        messageBuffer.flip();
        cut.send(messageBuffer);
        ByteBuffer receiveBuffer = ByteBuffer.allocate(1024);
        Set<SelectionKey> keys = cut.select();
        assertEquals("All interface channels must receive a message", keys.size(), size);
        for (SelectionKey key : keys) {
            DatagramChannel channel = (DatagramChannel) key.channel();
            channel.receive(receiveBuffer);
            receiveBuffer.flip();
            int sendType = receiveBuffer.getInt();
            assertEquals("Type must be equal to send type", 1, sendType);
            int messageSize = receiveBuffer.getInt();
            assertEquals("size sent must be equal to size received", messageBytes.length, messageSize);
            byte[] sentMessageBytes = new byte[messageSize];
            receiveBuffer.get(sentMessageBytes);
            String receivedMessage = new String(sentMessageBytes, ENCODING);
            assertEquals("Message sent is not the same as message received!", message, receivedMessage);
        }
    }

    @org.junit.Test
    public void canSendAndReceiveIDAndPortMessage() throws Exception {
        String id = "alfabetacain";
        int port = 42001;
        ByteBuffer toSend = MessageFactory.buildPortAndIdMessage(id, port);
        int size = cut.open();
        cut.send(toSend);
        Set<SelectionKey> keys = cut.select();
        for (SelectionKey key : keys) {
            ByteBuffer toReceive = ByteBuffer.allocate(1024);
            DatagramChannel channel = (DatagramChannel) key.channel();
            channel.receive(toReceive);
            toReceive.flip();
            IDAndPortMessage message = MessageFactory.readIdAndPortMessage(toReceive);
            assertEquals(id, message.getId());
            assertEquals(port, message.getPort());
        }
    }

    @org.junit.Test
    public void canSendTextMessage() throws Exception {
        String message = "Hello world!";
        TextMessage textMessage = new TextMessage(message);
        ByteBuffer buffer = textMessage.getByteBuffer();
        int size = cut.open();
        cut.send(buffer);
        Set<SelectionKey> keys = cut.select();
        for (SelectionKey key : keys) {
            ByteBuffer toReceive = ByteBuffer.allocate(1024);
            DatagramChannel channel = (DatagramChannel) key.channel();
            channel.receive(toReceive);
            toReceive.flip();
            TextMessage receivedTextMessage = new TextMessage(toReceive);
            assertEquals(textMessage.getMessage(), receivedTextMessage.getMessage());
        }
    }

    @org.junit.Test
    public void canSendCallMeMessage() throws Exception {
        CallMeMessage callMeMessage = new CallMeMessage(42000, "nickname");
        ByteBuffer buffer = callMeMessage.getByteBuffer();
        int size = cut.open();
        cut.send(buffer);
        Set<SelectionKey> keys = cut.select();
        for (SelectionKey key : keys) {
            ByteBuffer toReceive = ByteBuffer.allocate(1024);
            DatagramChannel channel = (DatagramChannel) key.channel();
            channel.receive(toReceive);
            toReceive.flip();
            CallMeMessage receivedCallMeMessage = new CallMeMessage(toReceive);
            assertEquals(42000, receivedCallMeMessage.getPort());
            assertEquals("nickname", receivedCallMeMessage.getName());
        }
    }
}